<?php

interface PersonInterface {
    public function run();

    public function getName();
}


class Person implements PersonInterface {

    private $name;

    /**
     * Constructor
     * @param string $name Person's name
     * @return void
     */
    public function __constructor(string $name): void {
        $this->name = $name;
    }

    /**
     * Start running
     * @return string
     */
    public function run(): string {
        echo 'Running';
    }

    /**
     * Get person's name.
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }
}


class Alien implements PersonInterface {

    private $name;

    /**
     * Constructor
     * @param string $name Person's name
     * @return void
     */
    public function __constructor(string $name): void {
        $this->name = $name;
    }

    /**
     * Start running
     * @return string
     */
    public function run(): string {
        echo 'Running';
    }

    /**
     * Get person's name.
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }
}

// Inheritance
class Woman {
    public $person;
    
    public function __constructor(PersonInterface $person): void {
        $this->person = $person;
    }
}


$person = new Person('Ivette');
$woman = new Woman($person);
echo $woman->getName(); // Ivette

$alien = new Alien('Foo');
$woman = new Woman($alien);
echo $woman->getName(); // Foo


