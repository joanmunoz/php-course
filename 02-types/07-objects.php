<?php
$obj = "[{
  'greeting': 'Hola',
  'foo': 'bar',
}]";

$obj = (object) ['greeting' => 'Hola', 'foo' => 'bar'];
/*
{
  'greeting': 'Hola',
  'foo': 'bar',
}
*/

echo $obj->greeting;
echo $obj->foo;

class Foo {
  public function do_foo() {
    echo 'Doing foo';
  }
}

$bar = new Foo();
$bar->do_foo();

