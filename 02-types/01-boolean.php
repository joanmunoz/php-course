<?php
$foo = true;
$foo = false;
?>

<p>The following values are considered <b>FALSE</b></p>

<ul>
  <li>The boolean false itself</li>
  <li>The integer 0 and -0</li>
  <li>The floats 0.0 and -0.0</li>
  <li>An array with zero element</li>
  <li>The special NULL (None - Python)</li>
</ul>

<h1>Converting to boolean</h1>

<h2>To explicitly convert a value to boolean, use the <i>(bool)</i> or <i>(boolean)</i> casts</h2>

<?php
var_dump((bool) "");
var_dump((bool) 1);

(int) ""; // 0
(float) 1; // 1.0
?>