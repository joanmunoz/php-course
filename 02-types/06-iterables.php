def foo(iterable):
    for value in iterable:
        print(value)

<?php

function foo(iterable $iterable = []): void {
    foreach ($iterable as $value) {
        echo $value;
    }
}

foo();
$arr = [1, 2, 3, 4];
foo($arr);

function bar(): iterable {
    return [1, 2, 3];
}

print_r(bar()); // [1, 2, 3]

function gen(): iterable {
    yield 1;
    yield 2;
    yield 3;
}

// 1
// 2
// 3
foreach (gen() as $value) {
    echo $value;
}

