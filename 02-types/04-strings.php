<h1>Single quoted and double quotes</h1>

<?php
$name = 'Joan';

echo 'this is a simple string $name';

echo 'I\'m Joan \n \t';

echo "I'm $name. Your user is {$name}Example \n \t";
?>

<h2>Heredoc</h2>

<?php 
$foo = <<<EOT
This is a multi line text \n \t



This is part of the same text
EOT;

echo $foo;
?>

<h1>Nowdoc</h1>

<?php 
$foo = <<<'BAR'
This is a multi line text \n \t



This is part of the same text
BAR;
?>