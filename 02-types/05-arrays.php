<?php

$array = array('a', 'b', 'c', 'd');

$array = ['a', 'b', 'c', 'd'];
echo $array[0]; // a

$array = [
  1 => "a",
  "1" => "b",
  1.5 => "c",
  true => "d",
];

echo $array[1]; // a
echo $array['1']; // b
echo $array[1.5]; // c
echo $array[true]; // d

// Dimensional
$array = [
  "foo" => "var",
  42 => 42,
  "multi" => [
    "dimensional" => [
      "array" => "foo",
    ],
  ],
];

echo $array["multi"]["dimensional"]["array"]; // foo

// Creating/Modifiying
$arr = [1, 2];

$arr[] = 56; // At the end
array_push($arr, 57); // At the end
array_unshift($arr, 60); // At the beginning
$arr['foo'] = 'bar';

echo $arr; // [0 => 60, 1 => 1, 2 => 2, 3 => 56, 4 => 57, 'foo' => bar];

unset($arr['foo']);

echo $arr; // [60, 1, 2, 56, 57];

unset($arr); // Delete a variable

$arr = [];
$array = (array) "[1, 2, 3, 4]";

$array = explode(',', "[1, 2, 3, 4]"); // ['[1', '2', '3', '4]']

echo $array[0]; //[
echo $array[1]; //1
echo $array[2]; //2
echo $array[3]; //3