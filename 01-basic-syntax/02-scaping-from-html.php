<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP - Escaping from HTML</title>
</head>
<body>
  <p>This is going to be ignored by PHP and display in the browser</p>

  <?php echo 'While this is going to be parsed.'; ?>
  
  <p>This will also be ignored by PHP.</p>
</body>
</html>
