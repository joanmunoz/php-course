<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Basic syntax</title>
</head>
<body>
  
  <ul>
    <li><a href="01-variables.php">Variables</a></li>
    <li><a href="02-scaping-from-html.php">Scaping from HTML</a></li>
    <li><a href="03-instruction-separation.php">Instruction separation</a></li>
    <li><a href="04-comments.php">Comments</a></li>
  </ul>

</body>
</html>